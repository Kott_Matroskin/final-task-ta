package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class LogInPage extends BasePage {

    public LogInPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@id=\"userid\"]")
    private  WebElement emailField;

    @FindBy(xpath = "//*[@id=\"signin-continue-btn\"]")
    private WebElement logInButton;

    public void IsEmailFieldVisible(){
        emailField.isDisplayed();
    }

    public void IsLogInButtonVisible(){
        logInButton.isDisplayed();
    }

    public void ClickLogInButton(){
        logInButton.click();
    }

    public void InputInEmailField(final String email){
        emailField.clear();
        emailField.sendKeys(email);
    }
}
