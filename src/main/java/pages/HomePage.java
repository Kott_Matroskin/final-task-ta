package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    //@FindBy(xpath = "//header[contains(@class, 'global-header global-header--sticky') or @class='page-header']")
    @FindBy(xpath = "//*[@id=\"gh\"]")
    private WebElement header;

    //@FindBy(xpath = "//footer")
    @FindBy(xpath = "//*[@id=\"glbfooter\"]")
    private WebElement footer;

    // @FindBy(xpath = ".//a[@class='global-header__main-bar__utility-nav__user-cart__link']")
    @FindBy(xpath = "//*[@id=\"gh-minicart-hover\"]/div/a[1]")
    private WebElement cartIcon;

    @FindBy(xpath = ".//a[contains(@class, 'header-top-bar__input__language')]/span")
    private WebElement languageButton;

    //@FindBy(xpath = "//button[contains(@class,'enterprise-account__button_sign-in')]")
    @FindBy(xpath = "//*[@id=\"gh-ug\"]/a")
    private WebElement signInButton;

    //@FindBy(xpath = "//button[contains(@class, 'enterprise-account__button_register')]")
    @FindBy(xpath = "//*[@id=\"gh-ug\"]")
    private WebElement registerSignInButton;



    @FindBy(xpath = "//*[@id=\"gh-shop-a\"]")
    private WebElement storeButton;

    @FindBy(xpath = "//*[@id=\"gh-shop\"]")
    private WebElement storePopup;

    @FindBy(xpath = "//*[@id=\"gh-ac\"]")
    private WebElement searchField;

    @FindBy(xpath = "//*[@id=\"gh-btn\"]")
    private WebElement searchButton;

    @FindBy(xpath = "//div[contains(@class,'wishlist-button')]//div[contains(@class,'items-count')]")
    private WebElement wishListProductsCount;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openHomePage(String url) {
        driver.get(url);
    }

    public void isHeaderVisible() {
        header.isDisplayed();
    }

    public void isFooterVisible() {
        footer.isDisplayed();
    }

    public void isCartIconVisible() {
        cartIcon.isDisplayed();
    }


    public void clickSignInButton() {
        signInButton.click();
    }

    public void isRegisterSignInButtonVisible() {
        registerSignInButton.isDisplayed();
    }


    public void clickStoreButton() {
        storeButton.click();
    }

    public boolean isStorePopupVisible() {
        return storePopup.isDisplayed();
    }

    public void isSearchFieldVisible() {
        searchField.isDisplayed();
    }

    public void isSearchFieldButtonVisible(){
        searchButton.isDisplayed();
    }

    public void clickCartButton() {
        cartIcon.click();
    }

    public void enterTextToSearchField(final String searchText) {
        searchField.clear();
        searchField.sendKeys(searchText);
    }

    public void clickSearchButton() {
        searchButton.click();
    }

    public WebElement getWishListProductsCount() {
        return wishListProductsCount;
    }

    public String getAmountOfProductsInWishList() {
        return wishListProductsCount.getText();
    }

}

