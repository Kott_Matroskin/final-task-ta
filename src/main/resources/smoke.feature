Feature: Smoke
  As a user
  I want to test all main site functionality
  So that I can be sure that site works correctly

  Scenario Outline: Check add product to wishlist
    Given User opens '<homePage>' page
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User check search button visibility
    And User clicks search button
    And User clicks wish list on first product
    Then User checks that amount of products in wish list are '<amountOfProducts>'
    Examples:
      | homePage              | keyword      | amountOfProducts |
      | https://www.ebay.com/ | 293705026301 | 1                |

  Scenario Outline: Check visibility of main elements website
    Given User opens '<homePage>' page
    And User checks header visibility
    And User checks footer visibility
    And User checks search field visibility
    And User checks cart visibility
    And User checks register sign-in button visibility
    And User checks search field visibility
    And User check search button visibility
    Then User checks that current url contains '<homePage>'

    Examples:
      | homePage              |
      | https://www.ebay.com/ |

  Scenario Outline: Check log-in user
    Given User opens '<homePage>' page
    And User checks register sign-in button visibility
    And User clicks 'Sign In' button
    And User entered '<email>' to the field
    And User click continue button

    Examples:
      | homePage              | email                  |
      | https://www.ebay.com/ | fobewo2698@beydent.com |

  Scenario Outline: Check add product to cart
    Given User opens '<homePage>' page
    And User checks search field visibility
    When User makes search by keyword '<keyword>'
    And User clicks search button
    And User clicks 'Add to Cart' button on product
    And User checks that add to cart popup visible
    And User checks 'Continue Shopping' button visibility
    And User checks 'Continue to Cart' button visibility
    Then User checks that add to cart popup header is '<header>'
    And User clicks 'Continue to Cart' button
    And User clicks 'Checkout' button
    And User clicks payment cart button
    And User checks payment form visibility
    And User checks billing form visibility
    And User checks 'Complete Order' button visibility

    Examples:
      | homePage              | keyword  | header                                |
      | https://www.ebay.com/ | 0830032p | You have added 1 item(s) to your cart |

